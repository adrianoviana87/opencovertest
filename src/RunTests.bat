setlocal enableextensions enabledelayedexpansion
@echo off
cd %1
REM Create a 'GeneratedReports' folder if it does not exist
if not exist "%~dp0GeneratedReports" mkdir "%~dp0GeneratedReports"
set cur_test_prj=0
set cur_target_prj=0

FOR /F %%t in ('dir /B/D *.Tests') DO (	
	set cur_test_prj=%%t
	set cur_target_prj=!cur_test_prj:~0,-6!
	
	CD %~dp0%%t
	
	call :RunClear		
	REM Run the tests against the targeted output
	call :RunOpenCoverUnitTestMetrics
	 
	REM Generate the report output based on the test results
	call :RunReportGeneratorOutput	
	
	call :RunClear		
)

CD %~dp0
call :RemoveTestOutput

REM Launch the report
if %errorlevel% equ 0 (
 call :RunLaunchReport
)

exit /b %errorlevel%
 
:RunOpenCoverUnitTestMetrics
"%~dp0\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe" ^
-register:user ^
-target:"%VS120COMNTOOLS%\..\IDE\mstest.exe" ^
-targetargs:"/testcontainer:\"%~dp0\!cur_test_prj!\bin\Debug\!cur_test_prj!.dll\" /resultsfile:\"%~dp0!cur_test_prj!.trx\"" ^
-filter:"+[!cur_target_prj!*]* -[*.Tests]*" ^
-mergebyhash ^
-skipautoprops ^
-output:"%~dp0\GeneratedReports\!cur_target_prj!Report.xml"
exit /b %errorlevel%
 
:RunReportGeneratorOutput
"%~dp0\packages\ReportGenerator.2.4.5.0\tools\ReportGenerator.exe" ^
-reports:"%~dp0\GeneratedReports\*Report.xml" ^
-targetdir:"%~dp0\GeneratedReports\ReportGenerator Output"
exit /b %errorlevel%
 
:RunLaunchReport
start "report" "%~dp0\GeneratedReports\ReportGenerator Output\index.htm"
exit /b %errorlevel%

:RunClear
REM Remove any previous test execution files to prevent issues overwriting
IF EXIST "%~dp0!cur_test_prj!.trx" del "%~dp0!cur_test_prj!.trx"
exit /b %errorlevel%
 
:RemoveTestOutput
REM Remove any previously created test output directories	
FOR /D /R %%X IN (%USERNAME%*) DO RD /S /Q "%%X" 
exit /b %errorlevel%