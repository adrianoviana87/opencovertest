﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyDomain.Tests
{
    [TestClass]
    public class PersonServiceTest
    {
        [TestMethod]
        public void CreateBaby_Couple_Baby()
        {
            var target = new PersonService();

            var husband = new Person
            {
                FirstName = "Alberto",
                LastName = "Roberto Pereira"
            };

            var wife = new Person
            {
                FirstName = "Maria",
                LastName = "Rosalva Juvenal"
            };
            
            var baby = target.CreateBaby("Ariovaldo", husband, wife);

            Assert.AreEqual("Ariovaldo", baby.FirstName);
            Assert.AreEqual("Juvenal Pereira", baby.LastName);
        }
    }
}
