﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDomain.Infra
{
    public static class Check
    {
        public static T NotNull<T>(T obj) where T : class
        {
            if (ReferenceEquals(null, obj))
            {
                throw new ArgumentNullException("obj");
            }

            return obj;
        }    
        
        public static T NotNull<T>(T? obj) where T :struct
        {
            if (!obj.HasValue)
            {
                throw new ArgumentNullException("obj");
            }

            return obj.Value;
        }        
    }
}
