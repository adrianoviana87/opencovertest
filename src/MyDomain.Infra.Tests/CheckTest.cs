﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MyDomain.Infra.Tests
{
    public class FakeClass
    {
        public string SomeProp { get; set; }
    }

    [TestClass]
    public class CheckTest
    {
        [TestMethod]
        public void NotNull_Instance_Value()
        {
            var expected = new FakeClass
            {
                SomeProp = "Test"
            };

            var actual = Check.NotNull(expected);
            Assert.AreSame(expected, actual);
        }
    }
}
