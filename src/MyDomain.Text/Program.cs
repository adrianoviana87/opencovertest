﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDomain.Text
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Husband: ");
            var husband = CreatePerson();
            Console.WriteLine("wife: ");
            var wife = CreatePerson();

            var babyName = ReadPrompt("Baby name: ");
            var baby = new PersonService().CreateBaby(babyName, husband, wife);
            Console.WriteLine("The Baby Is: ");
            Print(baby);
            Console.ReadKey();
        }

        static Person CreatePerson()
        {
            Person p = new Person();
            p.FirstName = ReadPrompt("First name: ");
            p.LastName = ReadPrompt("Last name: ");
            return p;
        }

        static void Print(Person person)
        {
            Console.WriteLine("First name: {0}", person.FirstName);
            Console.WriteLine("Last name: {0}", person.LastName);
        }

        static string ReadPrompt(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine();
        }
    }
}
