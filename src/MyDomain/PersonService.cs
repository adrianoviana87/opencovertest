﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDomain
{
    public class PersonService
    {
        public Person CreateBaby(string name, Person husband, Person wife)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new InvalidOperationException("Erro");
            }

            var baby = new Person();
            baby.FirstName = name;
            baby.LastName = GetLastName(wife.LastName) + " " + GetLastName(husband.LastName);
            return baby;
        }     
        
        private static string GetLastName(string name)
        {
            var words = name.Split(' ');
            if (words.Length == 1)
            {
                return words.First();
            }

            if (words.Contains("de"))
            {
                return string.Join(" ", words[words.Length - 2], words.Last());
            }

            return string.Empty;
        }   
    }
}
