﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class StringExtensions
    {
        public static string LastWord(this string str)
        {
            return str.Split(' ').Last();
        }                
    }
}
